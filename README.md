# burst_extraction

Script that identifies bursts in neural recordings.

Adapted from [DANC lab's burst detection
algorithm](https://github.com/danclab/burst_detection).

Summary: iterative identification of signal amplitude peaks in the time-frequency
domains following a TF decomposition like the Morlet Wavelet Transform, by
adjusting a threshold computed with the [FOOOF
algorithm](https://fooof-tools.github.io/fooof/). Consequently, these peaks are
associated with their corresponding troughs in the raw, time domain signals.

Designed to run on single channel/sensor data, with a few changes that make the
results more suitable for classification problems.

Changes:
1. Keep track of the experimental subject and channel/sensor from which bursts
originate.
2. Inform when skipping a trial with below aperiodic activity power.
3. Inform when a trial contains no bursts after setting the noise threshold.
4. Inform when a candidate burst is discarded because the raw signal phase has
no local minima.
5. If no bursts are detected pass an empty list as "waveform times".
